# Example QML4btag

This code implements a Variational Quantum Classifer for the b vs b-bar jet identificaiton. The model is based on a simple 4-qubit circuit consisting of a [Angle Embedding](https://pennylane.readthedocs.io/en/stable/code/api/pennylane.AngleEmbedding.html) layer and several [Strongly Entangling](https://pennylane.readthedocs.io/en/stable/code/api/pennylane.StronglyEntanglingLayers.html) layers.

## Setup
1. Install the required python packages
    ```
    pip install --upgrade pip
    pip install pennylane pandas numpy sklearn
    ```
2. Install jax

    __CUDA__
    ```
    pip install --upgrade "jax[cuda]" -f https://storage.googleapis.com/jax-releases/jax_releases.html
    ```
    
    __CPU__
    ```
    pip install --upgrade "jax[cpu]"
    ```
2. Extract the dataset files
    ```
    mkdir data
    tar xvf dataset.tar.xz -C data/
    ```
3. Run
    ```
    python train.py
    ```
## Tested on

- Stoomboot `wn-lot-008` node 2x Nvidia Tesla V100 GPUs (CUDA)
- Win11 WSL2 Intel i7-11800h (CPU)

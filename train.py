import pennylane as qml
from dataset import load_dataset
from functools import partial
import jax
from jax.example_libraries.optimizers import adam

SEED=0              # Fix it for reproducibility (kind of)
TRAIN_SIZE = 1000  # Number of jets (has to be even) for training
TEST_SIZE = 1000   # ------------------------------- for testing
N_QUBITS = 4        # One qubit per feature
N_LAYERS = 2        # Add more layers for extra complexity
LR=1e-3             # Learning rate of the ADAM optimizer
N_EPOCHS = 500     # Number of training epochs

# Definiton of the Pennylane device using JAX
device = qml.device("default.qubit.jax", wires=N_QUBITS,prng_key = jax.random.PRNGKey(SEED))

# Loads the dataset (already preprocessed... see dataset.py)
train_features,train_target,test_features,test_target = load_dataset(TRAIN_SIZE,TEST_SIZE,SEED)

# Definition of the quantum circuit
# x : features from the jet structure
# w : weights of the model
# The qml.qnode decorator transforms the python function into a Pennylane QNode
# i.e. a circuit to be run on a specified device.
# The partial(jax.vmap) decorator creates a vectorized version of the function
# This way I can process multiple jets at one time, passing a vector of features.
# in_axes = [0,None] specifies that I want to vectorize the function with respect
# to the first parameter only (x), since I want the weights (w) to be the same for
# each jet.
@partial(jax.vmap,in_axes=[0,None]) # Vectorized version of the function
@qml.qnode(device,interface='jax')  # Create a Pennylane QNode
def circuit(x,w):
    qml.AngleEmbedding(x,wires=range(N_QUBITS))   # Features x are embedded in rotation angles
    qml.StronglyEntanglingLayers(w,wires=range(N_QUBITS)) # Variational layer
    return qml.expval(qml.PauliZ(0)) # Expectation value of the \sigma_z operator on the 1st qubit

# Simple MSE loss function
def loss_fn(w,x,y):
    pred = circuit(x,w)
    return jax.numpy.mean((pred - y) ** 2)

# Simple binary accuracy function
def acc_fn(w,x,y):
    pred = circuit(x,w)
    return jax.numpy.mean(jax.numpy.sign(pred) == y)

loss = partial(loss_fn,x=train_features,y=train_target.to_numpy()) # Loss evaluated on the training dataset
acc = partial(acc_fn,x=test_features,y=test_target.to_numpy())     # Accuracy evaluated on the testing dataset

# Weights are initialized randomly
weights = jax.random.uniform(jax.random.PRNGKey(SEED), (N_LAYERS, N_QUBITS, 3))*jax.numpy.pi

# The ADAM optimizer is initialized
opt_init, opt_update, get_params = adam(LR)
opt_state = opt_init(weights)


# Training step
# This function is compiled Just-In-Time on the GPU
@jax.jit
def step(stepid, opt_state):
    current_w = get_params(opt_state)
    loss_value, grads = jax.value_and_grad(loss)(current_w)
    acc_value = acc(current_w)
    opt_state = opt_update(stepid, grads, opt_state)
    return loss_value,acc_value, opt_state


print("Epoch\tLoss\tAccuracy")
for i in range(N_EPOCHS):
    loss_value,acc_value, opt_state = step(i,opt_state)
    if (i+1) % 10 == 0:
        print(f"{i+1}\t{loss_value:.3f}\t{acc_value*100:.2f}%")


